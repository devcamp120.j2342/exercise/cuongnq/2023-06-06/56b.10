package com.devcamp.circlecylinderapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.model.Circle;
import com.devcamp.model.Cylinder;

@RestController
@RequestMapping("/")
@CrossOrigin

public class circlecontroller {
    @GetMapping("/circle-area")

    public double getArea(@RequestParam Double radius){
        Circle circle1 = new Circle(radius);
        return circle1.getArea();
    }

    @GetMapping("/cylinder-volume")

    public double getVolum(@RequestParam Double radius,@RequestParam Double height){
        Cylinder cylinder1 = new Cylinder(radius,height);
        return cylinder1.getVolum();
    }
}
